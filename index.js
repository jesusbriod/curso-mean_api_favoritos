'use strict'
var mongoose = require("mongoose");
var app = require("./app");
var port = process.env.PORT || 3000;

mongoose.connect('mongodb://localhost:27017/cursofavoritos',(err, res) => {
  if(err){
    throw err;
  } else {
    app.listen(port, () => {
      console.log('Conexión Exitosa a Mongodb!');
      console.log(`API REST favoritos... en el puerto ${port}`);
    });
  }


});
