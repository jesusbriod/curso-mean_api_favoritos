'use strict'

var Favorito  = require('../models/favorito');

function prueba (req, res) {
  if (req.params.nombre) {
    var nombre = req.params.nombre;

  }else {
    var nombre = "SIN NOMBRE";
  }
  res.status(200).send({data:[27, 11, 94], message: "Hola mundo con NodeJS y EXPRESS - "+nombre});
}

function getFavorito(req, res){
  var favoritoId = req.params.id;

  Favorito.findById(favoritoName, function(err, favorito){
    if (err) {
      res.status(500).send({message: 'Error al devolver el marcador'});
    }
    if(!favorito){
      res.status(404).send({message: 'No existe el marcador'});
    }
    res.status(200).send(favorito);
  });

}

function getFavoritos(req, res){
  Favorito.find({}).sort('-_id').exec((err, favoritos) => {
    if (err) {
      res.status(500).send({message: 'Error al devolver los marcadores'});
    }
    if(!favoritos){
      res.status(404).send({message: 'No hay marcadores'});
    }
    res.status(200).send(favoritos);
  });

}

function saveFavorito(req, res){
  var favorito = new Favorito();

  var params = req.body;

  favorito.title = params.title;
  favorito.description = params.description;
  favorito.url = params.url;
  favorito.save((err, favoritoStored) => {
    if (err) {
      res.status(500).send({message: 'Error al guardar el marcador'});
    }

    res.status(200).send({favorito: favoritoStored});
  });

}

function updateFavorito(req, res){
  var params = req.body;
  res.status(200).send({favorito: params});

}

function deleteFavorito(req, res){
  var favoritoId = req.params.id;
  res.status(200).send({data: favoritoId});

}

module.exports = {
  prueba,
  getFavorito,
  getFavoritos,
  saveFavorito,
  updateFavorito,
  deleteFavorito
}
